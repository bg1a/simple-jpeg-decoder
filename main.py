#! /usr/bin/python

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import binascii
import Image, ImageDraw

from jpg import *

TEST_FILE = './favjp.jpg'
OUT_FILE = 'outImage.bmp'

# Size: 16x16
outImg = Image.new("RGB", (16,16), "black");
draw = ImageDraw.Draw(outImg);

file = open(TEST_FILE, 'r');
fileBuffer = bytearray(file.read());
# Each section comprises of tag, length and value
imageSections = getAllSections(fileBuffer);
for num,i in enumerate(imageSections):
    print 'section %d:' % num, i[0], i[1], binascii.b2a_hex(i[2])

# Get start position of compressed data
dataIdx = reduce(lambda len, s2: len + s2[1] + 2, imageSections, 0);
dataLen = len(fileBuffer) - dataIdx;
bs = BitStream(fileBuffer[dataIdx:]);
print 'Data stream:\n', bs

# Get a component list from the first SOS section 
getSections = lambda sName, imageSections: \
        filter(lambda (tag, len, val): tag == sName, imageSections);
scanSections = getSections('SOS', imageSections);
assert len(scanSections) == 1, 'Single SOS section is accepted';
(sTag, sLen, sVal) = scanSections[0];
componentLst = getSOS(sVal, sLen - 2);

# Get DCT component list
dctSections = getSections('DCT', imageSections);
(dctTag, dctLen, dctVal) = dctSections[0];
dctComponentLst = getDCT(dctVal, dctLen - 2);

# Get Huffman tables
huffSections = getSections('DHT', imageSections);
# All Huffman tables are placed in hTables, like: DC0, AC0, DC1, AC1 and etc.
hTables = map(lambda (tag, len, val): getDHT(val, len - 2), huffSections);

# Get DQT tables
dqtSections = getSections('DQT', imageSections);
dqtTables = map(lambda (tag, len, val): getDQT(val, len - 2), dqtSections);

# Component number 0, Y component
Y = [];
(id, HDCtabIdx, HACtabIdx) = componentLst[0];
(id, horSp, verSp, DQTtabIdx) = dctComponentLst[0];
DC_HT = hTables[HDCtabIdx*2];
AC_HT = hTables[HACtabIdx*2 + 1];

for i in range(4):
    M = ZMatrix(8);
    # Parsing DC component
    DCvalue = parseDC(DC_HT, bs);
    if(None != DCvalue): M.insert(DCvalue);
    # Parsing AC component
    while bs.isValid():
        (ACzeroLen, ACcoef) = parseAC(AC_HT, bs);
        if(None != ACcoef):
            for zl in xrange(ACzeroLen): M.insert(0);
            M.insert(ACcoef);
        if(0 == ACcoef): break;

    DQTM = M.hadProduct(dqtTables[DQTtabIdx]);
    Y.append(getIDCT(DQTM));

# Component number 1, Cb component
(id, HDCtabIdx, HACtabIdx) = componentLst[1]; 
(id, CbHorSp, CbVertSp, DQTtabIdx) = dctComponentLst[1];
DC_HT = hTables[HDCtabIdx*2];
AC_HT = hTables[HACtabIdx*2 + 1];

M = ZMatrix(8);
# Parsing DC component
DCvalue = parseDC(DC_HT, bs);
if(None != DCvalue): M.insert(DCvalue);
# Parsing AC component
while bs.isValid():
    (ACzeroLen, ACcoef) = parseAC(AC_HT, bs);
    if(None != ACcoef):
        for zl in xrange(ACzeroLen): M.insert(0);
        M.insert(ACcoef);
    if(0 == ACcoef): break;

DQTCb = M.hadProduct(dqtTables[DQTtabIdx]);
Cb = getIDCT(DQTCb);

# Component number 2, Cr component
(id, HDCtabIdx, HACtabIdx) = componentLst[2]; 
(id, CrHorSp, CrVertSp, DQTtabIdx) = dctComponentLst[2];
DC_HT = hTables[HDCtabIdx*2];
AC_HT = hTables[HACtabIdx*2 + 1];

M = ZMatrix(8);
# Parsing DC component
DCvalue = parseDC(DC_HT, bs);
if(None != DCvalue): M.insert(DCvalue);
# Parsing AC component
while bs.isValid():
    (ACzeroLen, ACcoef) = parseAC(AC_HT, bs);
    if(None != ACcoef):
        for zl in xrange(ACzeroLen): M.insert(0);
        M.insert(ACcoef);
    if(0 == ACcoef): break;

DQTCr = M.hadProduct(dqtTables[DQTtabIdx]);
Cr = getIDCT(DQTCr);

# Define sweep order for image components:
Cidx = [(0,0), (1,0), (0,1), (1,1)];
for id, Cid in enumerate(Cidx):
    Ym = Y[id];
    for i in range(8):
        for j in range(8):
            Cbm = Cb[i/horSp + 4*Cid[0]][j/verSp + 4*Cid[1]];
            Crm = Cr[i/horSp + 4*Cid[0]][j/verSp + 4*Cid[1]];
            x = i + 8*Cid[0]; y = j + 8*Cid[1];
            color = getRGB(Ym[i][j], Cbm, Crm);
            draw.point((x,y), fill = color);

outImg.save(OUT_FILE, "BMP");

