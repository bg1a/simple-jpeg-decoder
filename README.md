Simple JPEG decoder
===================

Overview
--------
*main.py* script performs a decoding of a 16x16 pixel image (Google icon).
Math needed for the transformation is in *jpg.py*.
Actually, It was inspired by the brilliant article "Decoding JPEG for dummies" (in Russian)
from the Habrahabr site (http://habrahabr.ru/post/102521/). 

Usefull Link
------------
http://habrahabr.ru/post/102521/

http://www.swetake.com/photo/cr2/cr2-9_en.html

www.impulseadventure.com/photo/jpeg-huffman-coding.html

https://en.wikibooks.org/wiki/JPEG_-_Idea_and_Practice/The_compression_and_encoding

www.opennet.ru/docs/formats/jpeg.txt

