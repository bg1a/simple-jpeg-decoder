
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math

# Init a square matrix
newMatrix = lambda size: list([0]*size for i in xrange(size));

# Matrix with zig-zag order insertion
# http://rosettacode.org/wiki/Zig-zag_matrix
class ZMatrix:
    def __init__(self, size):
        self.__Matrix = newMatrix(size);
        self.__indexorder = sorted(((x,y) for x in range(size) for y in range(size)), \
                    key = lambda (x,y): (x+y, -y if (x+y) % 2 else y) )
        self.__idx = 0;
        self.__maxIdx = len(self.__indexorder);

    def get(self, i = None, j = None):
        'Get a value or a list of raws'
        if((None != i) and (None != j)):
            return self.__Matrix[i][j]
        else:
            return self.__Matrix;

    def insert(self, value):
        'Insert value into matrix'
        if(self.__idx < self.__maxIdx):
            i,j = self.__indexorder[self.__idx];
            self.__Matrix[i][j] = value;
            self.__idx += 1;

    def __str__(self):
        'String representation of a matrix'
        init = '';
        s = reduce(lambda s, next: s + str(next) + '\n', self.__Matrix, init);
        return s;
    
    # See: en.wikipedia.org/wiki/Matrix_multiplication#Hadamard_product
    def hadProduct(self, other):
        'Calculate Hadamard product of two matrices'
        result = self.__Matrix;
        if(isinstance(other, ZMatrix)):
            X = self.__Matrix;
            Y = other.__Matrix;
            result = map(lambda nRaw, mRaw: list(a*b for a,b in zip(nRaw, mRaw)), X, Y);
        return result;

# For convinience, create a matrix of cosine values.
# u (or v) and x (or y) will change through 0-7 interval, therefore
# we can precalculate all possible cos(x,u) values and store them into 
# this table
cosTable = newMatrix(8);
idctC = lambda idx: 1/math.sqrt(2) if (0 == idx) else 1; 
for x in range(8):
    for u in range(8):
        cosTable[x][u] = idctC(u) * math.cos((2*x + 1)*u*math.pi/16);

# Get single IDCT value
def sConv(rCoef, x, y):
    'Calculate single IDCT value'
    sum = 0;
    for u in range(8):
        for v in range(8):
            sum += rCoef[u][v] * cosTable[x][u] * cosTable[y][v];
    return sum;

# Get reconstructed pixel values
def getIDCT(rCoef):
    'Get IDCT matrix'
    S = newMatrix(8);
    for x in range(8):
        for y in range(8):
            S[y][x] = 0.25 * sConv(rCoef, x,y);
    return S;

# Get pixel RGB values from YCrCb
def getRGB(y, cb, cr):
    'Convert YCrCb into RGB'
    # Normalize component value
    normC = lambda c: 255 if c > 255 else (0 if (c < 0) else c);
    r = int(y + 1.402*cr + 128);
    g = int(y - 0.34414*cb - 0.71414*cr + 128);
    b = int(y + 1.772*cb + 128);
    return (normC(r), normC(g), normC(b));

# JPG file section identificators
JPGsections = {
        'Start' : 0xffd8,
        'Comment' : 0xfffe,
        # Quantization table
        'DQT' : 0xffdb,
        # Baseline DCT, 
        'DCT' : 0xffc0,
        # Huffman table, DHT
        'DHT' : 0xffc4,
        # Start of scan
        'SOS' : 0xffda,
        }

# Get Short integer from raw buffer
getUint16FromBuf = lambda buffer: (buffer[0] << 8) + buffer[1];

# Get JPG file section
def getNextSection(buffer):
    'Get section data from buffer, where image is dumped'
    sectionName = 'Unknown';
    sectionID = getUint16FromBuf(buffer);
    sectionSize = 0;
    sectionData = bytearray();
    for section in JPGsections:
        if JPGsections[section] == sectionID:
            sectionName = section;
            if section != 'Start':
                sectionSize = getUint16FromBuf(buffer[2:]);
                sectionData = buffer[4 : 2 + sectionSize];
    return sectionName, sectionSize, sectionData

# Get all JPG sections
def getAllSections(fileBuffer):
    'Get all JPG sections'
    cnt = 0;
    sectionList = [];
    while cnt < (len(fileBuffer) - 2):
        dsc = getNextSection(fileBuffer[cnt:]);
        if dsc[0] == 'Unknown':
            break;
        sectionList.append(dsc);
        cnt += dsc[1] + 2;
    return sectionList;

# Get DQT from raw buffer
def getDQT(buffer, dqtSize):
    'Convert raw DQT into a matrix'
    elementSize = buffer[0] & 0xf0;
    tableID = buffer[0] & 0x0f;
    # We deal with square matrix
    size = int(dqtSize**0.5 + 0.5);
    dqt = ZMatrix(size);
    # Support only a 1-byte values for DQT
    if 0 == elementSize:
        for i in xrange(1, dqtSize):
            dqt.insert(buffer[i]);
    else:
        print 'Only 1-byte values are supported for DQT'
    return dqt;

# Get DCT from raw buffer
def getDCT(buffer, dctSize):
    'Get DCT components from buffer'
    precision = buffer[0];
    height = getUint16FromBuf(buffer[1:]);
    width = getUint16FromBuf(buffer[3:]);
    components = buffer[5];
    componentsList = []; 
    for i in range(components):
       id = buffer[i*3 + 6];
       hor = (buffer[i*3 + 7] & 0xf0) >> 4;
       ver = buffer[i*3 + 7] & 0x0f;
       dqtId = buffer[i*3 + 8];
       componentsList.append((id, hor, ver, dqtId));
    return componentsList;

# Get Huffman tables from raw buffer
def getDHT(buffer, dhtSize):
    'Get Huffman codes'
    getPref = lambda value, valueBitlen, prefBitlen: value >> (valueBitlen - prefBitlen);
    codesArrayLen = 16;
    tableClass = (buffer[0] & 0xf0) >> 4;
    tableID = (buffer[0] & 0x0f);
    # This array contains list for each bitlength (1-16)
    # Each list contains tuple of code-value pair.
    # [[(0, 1), (1, 3)], [...], ...]
    codeToValArray = [[(0,0)]]*(codesArrayLen + 1);
    valueIdx = codesArrayLen + 1;
    if buffer[1] > 0:
        codeToValArray[1] = [(0, buffer[valueIdx])]; valueIdx += 1;
        if 2 == buffer[1]:
            codeToValArray[1].append((1, buffer[valueIdx])); valueIdx += 1;
    lastNonEmptySetIdx = 1;
    for i in range(2, codesArrayLen + 1):
        # if we have an entire set of codes for previous bitlength, so
        # exit this loop, because it's imposible to generate unique code
        # which does not contain prefix from the previous set
        if 2**(lastNonEmptySetIdx) == len(codeToValArray[lastNonEmptySetIdx]):
            break;
        codesNum = buffer[i];
        # skip empty set
        if 0 == codesNum: 
            continue;
        # Check if a prefix from generated value is present in an array of codes 
        isPrefInArray = lambda inPref, arrIdx : any(map(lambda codeToVal : \
                codeToVal[0] == inPref, codeToValArray[arrIdx]));
        # filter out value which consists a prefix from the previous set
        isCodeHasNoPref = lambda inCode : not any(map(lambda arrIdx : \
            isPrefInArray(getPref(inCode, i, arrIdx), arrIdx), range(1, lastNonEmptySetIdx + 1)));
        codes = filter(isCodeHasNoPref, xrange(2**(i - 1), 2**i))
        # Add in the current position code-value pairs
        codeToValArray[i] = map(None, codes[:codesNum], buffer[valueIdx:valueIdx + codesNum]);
        valueIdx += codesNum;
        lastNonEmptySetIdx = i;
    return codeToValArray;

# Get Start of scan from raw buffer
def getSOS(buffer, sSize):
    'Get components from SOS section'
    componentsNum = buffer[0];
    componentsList = [0]*componentsNum;
    for i in range(componentsNum):
        componentId = buffer[i*2 + 1];
        DCtabId = (buffer[i*2 + 2] & 0xf0) >> 4;
        ACtabId = buffer[i*2 + 2] & 0xf;
        componentsList[i] = (componentId, DCtabId, ACtabId);
    return componentsList;

# A data stream abstraction
class BitStream:
    def __init__(self, buffer):
        # copy input buffer
        self.__data = bytearray(x for x in buffer);
        self.__bitLen = len(self.__data) * 8;
        self.__rem = self.__bitLen;
   
    # Check next bitNum bits in a stream
    def peek(self, bitNum):
        'Get bit number from bit stream, do not change stream position'
        assert bitNum <= 16, 'Can only process bitfield of length less than 16'
        bitN = bitNum;
        bitfield = 0;
        bitIdx = self.__bitLen - self.__rem;
        byteIdx = bitIdx / 8;
        if(bitN > self.__rem):
            bitN = self.__rem;
      
        if(bitN > 0):
            d = 8 - (bitIdx % 8);
            mask = (1 << bitN) - 1;
            byte = self.__data[byteIdx];
            nextByte = self.__data[byteIdx + 1];
            lastByte = self.__data[byteIdx + 2];
            if(d >= bitN):
                bitfield = byte >> (d - bitN);    
            else:
                if(8 < (bitN - d)):
                    bitfield = lastByte >> (16 - bitN + d);
                    bitfield |= nextByte << (bitN - d - 8);
                else:
                    bitfield = nextByte >> (8 - bitN + d);
                bitfield |= byte << (bitN - d); 
            bitfield &= mask;
        return (bitN, bitfield);

    # Extract bits from stream
    def get(self, bitNum):
        'Get bits, change stream position'
        bitN = bitNum;
        if(bitN > self.__rem):
            bitN = self.__rem;
        bits = self.peek(bitN);
        self.__rem -= bitN;
        return bits;

    # Skip next bitNum bits in a stream
    def skip(self, bitNum):
        'Skip bit sequence'
        bitN = bitNum;
        if(bitN > self.__rem):
            bitN = self.__rem;
        self.__rem -= bitN;

    # Print remaining bytes from a stream
    def __str__(self):
        'Convert bitstream into string'
        fmt = '08b';
        dataIdx = (self.__bitLen - self.__rem) / 8;
        initVal = format(self.__data[dataIdx], fmt);
        dataList = self.__data[dataIdx + 1:];
        str = reduce(lambda prev, next: prev + format(next, fmt), dataList, initVal);
        return str;

    # Check if there is any remaining bits in a stream
    def isValid(self):
        'Check if bitstream is not ended'
        return (0 != self.__rem);

# Normalize DC/AC coefficients
def normCoef(value, bitlen):
    'Normalize coefficient value'
    normValue = -1;
    MSbit = 0;
    if(bitlen > 0):
        MSbit = value & (1 << (bitlen - 1));
        if(0 == MSbit):
            normValue = value - 2**bitlen + 1;
        else:
            normValue = value;
    return normValue;

# Get a DC coefficient from data bitsream
def parseDC(DCHuffmanTable, bitstream):
    'Parse a DC coefficient'
    DCcoef = None;
    for bitlen in range(1, len(DCHuffmanTable)):
        bitN,bits = bitstream.peek(bitlen);
        htItems = filter(lambda (code, value): bits == code, DCHuffmanTable[bitlen]);
        if (0 != len(htItems)):
            (code, coefLen) = htItems[0];
            bitstream.skip(bitlen);
            if(0 != coefLen):
                bitN, DCvalue = bitstream.get(coefLen);
                DCcoef = normCoef(DCvalue, coefLen);
            else:
                DCcoef = 0;
            break;
    return DCcoef;
           
# Get an AC coefficient from data bitsream
def parseAC(ACHuffmanTable, bitstream):
    'Parse a AC coefficient'
    ACcoef = None;
    ACzeroLen = None;
    for bitlen in xrange(1, len(ACHuffmanTable)):
        bitN,bits = bitstream.peek(bitlen);
        htItems = filter(lambda (code, value): bits == code, ACHuffmanTable[bitlen]);
        if (0 != len(htItems)):
            (code, ACs) = htItems[0];
            bitstream.skip(bitlen);
            if(0 != ACs):
                ACzeroLen = (ACs & 0xf0) >> 4; 
                coefLen = ACs & 0xf;
                bitN, ACvalue = bitstream.get(coefLen);
                ACcoef = normCoef(ACvalue, coefLen);
            else:
                ACzeroLen = 0;
                ACcoef = 0;
            break;
    return (ACzeroLen, ACcoef);

